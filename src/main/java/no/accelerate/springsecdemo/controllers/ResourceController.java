package no.accelerate.springsecdemo.controllers;

import no.accelerate.springsecdemo.models.dtos.ResponseMessage;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
// allow origins to access api
@CrossOrigin("*") // TODO specify this later on
//@CrossOrigin(
//        origins = {
//                "http://localhost:4200"
//        }
//)
@RequestMapping("api/")
public class ResourceController {

    @GetMapping("public")
    public ResponseEntity getPublic() {
        ResponseMessage message = new ResponseMessage();
        message.setMessage("Public resources");
        return ResponseEntity.ok(message);
    }

    @GetMapping("private")
    public ResponseEntity getProtected() {
        ResponseMessage message = new ResponseMessage();
        message.setMessage("Protected resources");
        return ResponseEntity.ok(message);
    }

//    @GetMapping("scope")
//    public ResponseEntity getProfile() {
//        ResponseMessage message = new ResponseMessage();
//        message.setMessage("Scope protected resources");
//        return ResponseEntity.ok(message);
//    }

    @GetMapping("role")
    //@PreAuthorize("hasRole(ADMIN)") - instead of mvcMatcher().hasRole
    public ResponseEntity getAdmin() {
        ResponseMessage message = new ResponseMessage();
        message.setMessage("Role protected resources");
        return ResponseEntity.ok(message);
    }

    @GetMapping("{id}")
    public ResponseEntity getUserResources(@PathVariable String id, @AuthenticationPrincipal Jwt jwt) {
        // Protect a users resources! Only you can view your resources
        if(id != jwt.getClaimAsString("sub"))
            return ResponseEntity
                    .status(HttpStatus.FORBIDDEN)
                    .body("User ID does not match");
        ResponseMessage message = new ResponseMessage();
        message.setMessage("Resources for user: " + id);
        return ResponseEntity.ok(message);
    }
}
