package no.accelerate.springsecdemo.models.dtos;

import lombok.Data;

@Data
public class ResponseMessage {
    private String message;
}
